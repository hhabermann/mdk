UCS in VM installieren
======================

* VM erstellen
	* Festplatte: 25GB
	* 2GB Ram
	* Netzwerk: Netzwerkbr�cke
	* Audio: deaktiviert
* UCS �ber ISO installieren
	* Beim Booten: "start with manual network settings"
* Statische IP (192.168.1.1<UCS-Version> z.B. 192.168.1.150 f�r 5.0)



Nach Installation
=================
	* passwd root (test)
	* apt-get update; apt-get upgrade -y; apt-get clean
	* dd if=/dev/zero of=/z; rm /z; poweroff
	* Als OVA exportieren (z.B. UCS 5.0 - frisch installiert.ova)
	* Sicherungspunkt "vor" erstellen
	* VM klonen als UCS x.y - UCS-Repo
		* Vollst�ndiger Klon
		* alles (kein aktueller Zustand)
	* Original-VM umbenennen: z.B. UCS 5.0 - Lokales Testrepo



Transfer auf T
==============
	* Beide VMs exportieren und auf T importieren
	* Netzwerkkarten umstellen, da die Bezeichnung des Netzwerkbr�ckenger�tes abweicht
	* Dann den Sicherungspunkt "vor" erstellen




m23-autoTest anpassen
=====================

* Funktion CATSG::initServerArray in /mdk/autoTest/autoTestScriptGenerator.php editieren:
	* ggf. alte UCS-Version aus dem Array nehmen bzw. durch neue ersetzen
	* Neue UCS-Version (als "Lokales Testrepo" und "UCS-Repo") eintragen (IP!)

* ./autoTestScriptGenerator.php aufrufen, um neue Skripte zu erstellen
	* aTS-UCSx.y-LokalesTestrepo.sh und aTS-UCSx.y-UCS-Repo.sh ansehen