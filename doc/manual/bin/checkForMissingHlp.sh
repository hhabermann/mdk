#!/bin/bash

. /mdk/bin/forkFunctions.inc

isDevelReturn
if [ $? -ne 0 ]
then
	echo "ERROR: Can only be run, if the development version is active"
	exit 23
fi



# Temporary directory to store the old and new files
tmp="/tmp/missingHelp"
rm -r "$tmp" 2> /dev/null

cd /m23

# Get the name of the m23 release branch
branch=$(getm23ReleaseBranchName)

# Get the creation time of the m23 release branch. This is the last time the translation was complete
creationTime=$(git log -r $branch | grep 'Date:' | head -1 | sed 's/Date://')


# Create the output directories
for lang in de en fr
do
	mkdir -p "$tmp/old/help/$lang" "$tmp/new/help/$lang"
	mkdir -p "$tmp/old/i18n/$lang" "$tmp/new/i18n/$lang"
done


# Get the German help files that differ from release to development version
# List all German translated files that were changed since the last release
git whatchanged --since "$creationTime"  --oneline --name-only --pretty=format: | sort | uniq | egrep '(inc/help/de|inc/i18n/de)' | while read deName
do
	echo $deName

	for lang in de en fr
	do
		fullname=$(echo $deName | sed "s#/de/#/$lang/#")
		echo $fullname
	
		cp -v "/m23/$fullname" "$tmp/new/help/$lang"
		basename=$(basename $fullname)
		git show "$branch:$fullname" > "$tmp/old/help/$lang/$basename"
	done
done



# Write a script for starting meld
echo "#!/bin/bash
meld old new" > $tmp/run-meld
chmod +x $tmp/run-meld



# Make an archive and delete the temp files
cd "$tmp"
tar cfvz ../m23-i18n-help-translate.tar.gz .
cd ..
rm -r "$tmp"

echo "Translation files stored in /tmp/m23-i18n-help-translate.tar.gz"