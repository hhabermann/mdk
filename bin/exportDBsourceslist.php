<?php

//Called by /mdk/m23Debs/bin/mkm23Deb

include("/m23/inc/capture.php");
include("/m23/inc/db.php");
dbConnect();

// Fields to export from sourceslist
$exportFields = array("name", "distr", "release", "description", "list", "desktops", "archs");

// Names of sourceslists that should get exported
$allowedNames = array("imaging", "stretch", "LinuxMint 18.3 Sylvia", "Ubuntu-Bionic", "LinuxMint 19 Tara", "LinuxMint 19.1 Tessa", "buster", "LinuxMint 19.2 Tina", "LinuxMint 19.3 Tricia", "Ubuntu-Focal", "LinuxMint 20 Ulyana", 'bullseye', "LinuxMint 20.2 Uma", "LinuxMint 20.3 Una", "Ubuntu-Jammy", "LinuxMint 21 Vanessa");

// Set to false, to invert the list of allowed sourcelist names
$shouldBeInArray = true;

// Check, if one or many parameters are given
if ($argc > 1)
{
	// Invert the list of allowed names, if parameter "nonStandardSourceslists" is given as 1st command line parameter
	if ($argv[1] == 'nonStandardSourceslists')
		$shouldBeInArray = false;
	else
	// Export only sourceslists with names given as command line parameters
		$allowedNames = $argv;
}

$exportSql = $fieldSql = "";

//SQL string of all fields 
foreach ($exportFields as $field)
	$fieldSql .= "`$field`, ";
$fieldSql = rtrim($fieldSql,", ");

//create SQL query to export all fields from sourceslist
$exportSql .= "SELECT $fieldSql FROM sourceslist";

$result=db_query($exportSql);

//lock table for write operations
$out = "LOCK TABLES `sourceslist` WRITE;\n";
while ($row = mysqli_fetch_assoc($result))
{
	//check if the sourceslist should get exported
	if (in_array($row["name"],$allowedNames) == $shouldBeInArray)
	{
		//delete sourceslist with the same name
		$out .= "DELETE FROM `sourceslist` WHERE name=\"$row[name]\";\n";

		//export data
		$out .= "INSERT INTO `sourceslist` ($fieldSql) VALUES (";
		foreach ($exportFields as $field)
		{
			$val = str_replace("'", "\'", $row[$field]);
			$out .= "'$val', ";
		}
		$out = rtrim($out,", ");
		$out .= ");\n";
	}
}
$out .= "UNLOCK TABLES;";

echo($out);
?>