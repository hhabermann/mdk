#!/bin/bash

# List of directories that should be saved/restored when switching between release and development version
export specialDirs="/mdk/server/iso/pool /m23/db /m23/tftp /mdk/m23Debs/debs /mdk/client"




#####
# name ensureDevelopmentVersion
# description Ensure, the development version is set in version.php
#####
ensureDevelopmentVersion()	# git
{
	# Ensure, the development version is set
	sed -i 's/rock"/rock-devel"/' /m23/inc/version.php
}





#####
# name ensureReleaseVersion
# description Ensure, the release version is set in version.php
#####
ensureReleaseVersion()	# git
{
	# Ensure, the release version is set
	sed -i 's/rock-devel/rock/' /m23/inc/version.php
}





#####
# name meldDevelopmentReleaseBranch
# description Melds changes from release or master into the current /m23 or /mdk
# parameter: m23 or mdk for meld with /m23 or /mdk
#####
meldDevelopmentReleaseBranch()	# git
{
	export type=$1

# 	tempDir="/tmp/$type"
# 	rm -r "$tempDir" 2> /dev/null
	
	if [ $type == 'm23' ]
	then
		export branch="$(getm23ReleaseBranchName)"
	else
		export branch="$(getMDKReleaseBranchName)"
	fi

	# Check availability for m23 or MDK release branch
	if [ -z "$branch" ]
	then
		echo "No release branch for $type available!"
		read -p'Press Enter to continue'
		return
	fi


	isDevelReturn
	if [ $? -eq 0 ]
	then
		cd /$type
		
		if [ $(git diff --name-status "$branch" | wc -l) -eq 0 ]
		then
			read -p"No changes between development/master and release branch found!

Press Enter to continue"
		else
			read -p"Left: Release branch. Right: Local development version in /$type

To accept changes copy from left => right

Press Enter to continue"
			git difftool -d -t kompare "$branch"
		fi
		# Checkout release branch to temp dir
		#git clone -b "$branch" "/$type" "$tempDir"
		
		# Ensure, the development version is set
		ensureDevelopmentVersion
	else
		cd /$type
		if [ $(git diff --name-status master | wc -l) -eq 0 ]
		then
			read -p"No changes between release and development/master branch found!

Press Enter to continue"
		else
			read -p"Left: Development/master branch. Right: Local release version in /$type

To accept changes copy from left => right

Press Enter to continue"
			git difftool -d -t meld master
		fi
		# Checkout development branch to temp dir
		#git clone -b master "/$type" "$tempDir"

		# Ensure, the release version is set
		ensureReleaseVersion
	fi
	
	
}




#####
# name checkUncommited
# description Checks for uncommited changes in git and exits, if there were found any.
# parameter: Directory to check for changes
#####
checkUncommited()	# git
{
	cd "$1"
	if [ $(git status --porcelain | wc -l) -gt 0 ]
	then
		echo "ERROR: Uncommited changes in $(pwd)."
		read -p 'Press Enter'
		exit 23
	fi
}





#####
# name switchDevel
# description Switches to the development branches of m23/MDK
#####
switchDevel()	# git
{
	rootRequiredDialog

	isDevelReturn
	if [ $? -eq 0 ]
	then
		echo "m23/MDK are on development branch already!"
		read -p'Press Enter to continue'
		return
	fi

	# Check for uncommited changes
	checkUncommited /m23
	checkUncommited /mdk

	# Checkout development/master branch in /m23
	cd /m23
	sudo -u dodger sh -c "git checkout master"
	if [ $? -ne 0 ]
	then
		echo "Could not switch to development/master branch in /m23"
		read -p'Press Enter to continue'
		return
	fi

	# Checkout development/master branch in /mdk
	cd /mdk
	sudo -u dodger sh -c "git checkout master"
	if [ $? -ne 0 ]
	then
		echo "Could not switch to development/master branch in /mdk"
		read -p'Press Enter to continue'
		return
	fi

	# Ensure, the development version is set
	ensureDevelopmentVersion

	# Change special directories to development version
	switchSpecialDirs switchDevel

	read -p'Press Enter to continue'
}





#####
# name switchRelease
# description Switches to the release branches of m23/MDK.
#####
switchRelease()	# git
{
	rootRequiredDialog

	isDevelReturn
	if [ $? -ne 0 ]
	then
		echo "m23/MDK are on release branch already!"
		read -p'Press Enter to continue'
		return
	fi

	# Check availability for m23 release branch
	if [ -z "$(getm23ReleaseBranchName)" ]
	then
		echo "No release branch for m23 available!"
		read -p'Press Enter to continue'
		return
	fi

	# Check for uncommited changes
	checkUncommited /m23
	checkUncommited /mdk

	# Checkout latest release branch in /m23
	cd /m23
	sudo -u dodger sh -c "git checkout $(getm23ReleaseBranchName)"
	if [ $? -ne 0 ]
	then
		echo "Could not switch to release branch in /m23"
		read -p'Press Enter to continue'
		return
	fi

	# Check availability for MDK release branch
	if [ -z "$(getMDKReleaseBranchName)" ]
	then
		echo "No release branch for MDK available!"
		read -p'Press Enter to continue'
		return
	fi

	# Checkout latest release branch in /mdk
	cd /mdk
	sudo -u dodger sh -c "git checkout $(getMDKReleaseBranchName)"
	if [ $? -ne 0 ]
	then
		echo "Could not switch to release branch in /mdk"
		read -p'Press Enter to continue'
		return
	fi

	# Ensure, the release version is set
	ensureReleaseVersion

	# Change special directories to release version
	switchSpecialDirs switchRelease

	read -p'Press Enter to continue'
}





#####
# name getm23ReleaseBranchName
# description Shows the name of the m23 release branch
#####
getm23ReleaseBranchName()	# git
{
	cd /m23
	git branch -a | egrep 'm23-[2-9][0-9]\.[1-9]' | sed -e 's/^\*//' -e 's/^ *//' | grep -v remotes | sort -n | tail -1
}





#####
# name getMDKReleaseBranchName
# description Shows the name of the mdk release branch
#####
getMDKReleaseBranchName()	# git
{
	cd /mdk
	git branch -a | egrep 'mdk-[2-9][0-9]\.[1-9]' | sed -e 's/^\*//' -e 's/^ *//' | grep -v remotes | sort -n | tail -1
}





#####
# name rootNotAllowedDialog
# description Check if the script is run as root and if yes, show an error message and quit the script
#####
rootNotAllowedDialog()	# git
{
	if [ $(whoami) == 'root' ]
	then
		dialog --title 'Non-root user required' --msgbox "Operation not allowed as root user!\n\nSwitch to your development user and try again." 7 50
		exit
	fi
}





#####
# name rootRequiredDialog
# description Check if the script is run as root and if no, show an error message and quit the script
#####
rootRequiredDialog()	# git
{
	if [ $(whoami) != 'root' ]
	then
		dialog --title 'Root user required' --msgbox "Operation requires root user!\n\nSwitch to root user and try again." 7 50
		exit
	fi
}





#####
# name makeRelease
# description Shows a dialog for entering the version of the new m23/MDK release branch and creates both branches
#####
makeRelease()	# git
{
	rootRequiredDialog

	# Read the version number from version.php as the default number
	default="$(grep m23_version /m23/inc/version.php | cut -d'"' -f2)"

	# Set an invalid version number
	export version=bla

	# Ask for a version number as long as it is unvalid
	while [ $(echo $version | egrep '[2-9][0-9]\.[1-9]' -c) -eq 0 ]
	do
		# Show a dialog to enter the wanted version number for the release branch
		export version=$(dialog --title "m23 version for new release branch" --inputbox 'It will create new branches with the schema "m23-<version>" + "mdk-<version>" . Enter the m23 version for new release branch:' 10 80 $default 3>&1 1>&2 2>&3 3>&- )
	done

	# Make the release branch in /m23
	cd /m23
	sudo -u dodger sh -c "git branch -v 'm23-$version'"
	if [ $? -eq 0 ]
	then
		echo "Branch \"m23-$version\" was created in /m23"
	fi
	sudo -u dodger sh -c "git push -u origin 'm23-$version'"

	# Make the release branch in /mdk
	cd /mdk
	sudo -u dodger sh -c "git branch -v 'mdk-$version'"
	if [ $? -eq 0 ]
	then
		echo "Branch \"mdk-$version\" was created in /mdk"
	fi
	sudo -u dodger sh -c "git push -u origin 'mdk-$version'"

	# Outdates the cache directories and makes a release copy of the current (development) directories.
	copym23MDKToCacheDir

	read -p'Press Enter to continue'
}





#####
# name getNextPatchlevel
# description Shows the next available patch number.
#####
getNextPatchlevel()	# git
{
	# Check, if /mdk and /m23 are both on a release or development version
	isDevelReturn

	cd /m23
	
	# Variable to store the highest detected m23_patchLevel
	export last=0
	
	# Run thru all local branches
	for branch in $(git branch -a | grep -v remotes/origin | sed -e 's/^\*//' -e 's/^ *//')
	do
		# Get m23_patchLevel from version.php of the branch
		v=$(git show $branch:inc/version.php | grep m23_patchLevel | cut -d'"' -f2)
	
		# Update the patch level variable, if the branch has a higher m23_patchLevel
		if [ $v -gt $last ]
		then
			export last=$v
		fi
	done

	# Check, if there is an even higher number in version.php
	v="$(grep m23_patchLevel /m23/inc/version.php | cut -d'"' -f2)"
	if [ $v -gt $last ]
	then
		export last=$v
	fi

	expr $last \+ 1
}





#####
# name forkStatusMessage
# description Generates a status text with the currently active fork (development or release), the release and development directories of m23/MDK.
#####
forkStatusMessage()	# git
{
	isDevelReturn
	if [ $? -eq 0 ]
	then
		mode='development (master branch)'
	else
		mode='release'
	fi

	echo "Mode: $mode"
	echo "m23 release branch: $(getm23ReleaseBranchName)"
	echo "MDK release branch: $(getMDKReleaseBranchName)"
}





#####
# name startDaemons
# description Starts the MySQL and Apache 2 daemons.
#####
startDaemons()	# git
{
	/etc/init.d/apache2 start
	/etc/init.d/mysql start
	/etc/init.d/tftpd-hpa start
}





#####
# name stopDaemons
# description Stops the MySQL and Apache 2 daemons.
#####
stopDaemons()	# git
{
	/etc/init.d/apache2 stop
	/etc/init.d/mysql stop
	/etc/init.d/tftpd-hpa stop
}





#####
# name isDevelReturn
# description Checks if /m23 and /mdk are using the git master branch (development version).
# returns: 0, if in development mode, with 1, when in normal mode.
#####
isDevelReturn()	# git
{
	# Test, if git in /m23 is on master branch (yes = development)
	cd /m23; [ $(LC_ALL=C git status | grep -i branch | grep -i master -c) -gt 0 ]; ism23Devel=$?

	# Test, if git in /mdk is on master branch (yes = development)
	cd /mdk; [ $(LC_ALL=C git status | grep -i branch | grep -i master -c) -gt 0 ]; ismdkDevel=$?

	# Check, if /m23 and /mdk use differently named branches
	if [ $ism23Devel -ne $ismdkDevel ]
	then
		if [ $ism23Devel -eq 0 ]
		then
			errmsg='/m23 is on master branch aka. development version,'
		else
			errmsg='/m23 is on a release branch aka. release version,'
		fi

		if [ $ismdkDevel -eq 0 ]
		then
			errmsg="$errmsg /mdk is on master branch aka. development version."
		else
			errmsg="$errmsg /mdk is on a release branch aka. release version."
		fi

		echo "Err: $errmsg. A mismatch should NOT happen here."
		exit 5
	fi

	#Check, if /m23 and /mdk are both on developmen branch
	if [ $ism23Devel -eq 0 ] && [ $ismdkDevel -eq 0 ]
	then
		return 0
	else
		return 1
	fi
}





#####
# name origDir2CacheDir
# description Converts a directory name in the file system (eg. /m23/tftp) to the according cache directory (eg. /mdk/specialDirs/m23-tftp)
# parameter: Original directory with full path
#####
origDir2CacheDir()
{
	echo $1 | sed -e 's#/#-#g' -e "s#^-#/mdk/specialDirs/#"
}





#####
# name copym23MDKToCacheDir
# description Outdates the cache directories and makes a release copy of the current (development) directories.
#####
copym23MDKToCacheDir()
{
	# Create a new directory for the now outdated cache directories
	outdatedDir=$(date +"/mdk/outdated/specialDirs-%F_%H-%M")
	mkdir -p "$outdatedDir"

	# Move the now outdated cache directories to the outdated directory
	mv -v /mdk/specialDirs/* "$outdatedDir"

	# Run thru the directories that have to be cached
	for origDir in $specialDirs
	do
		# Calculate the name of the directory where the version is stored that currently is NOT in use
		cacheDir="$(origDir2CacheDir $origDir)"
		cacheStateFile="$cacheDir/releaseOrDevel"

		# Copy of the current directory to the cache
		cp -rav "$origDir" "$cacheDir"

		# Mark the copy as release version
		echo -n 'release' > "$cacheStateFile"
	done
}





#####
# name switchSpecialDirs
# description Switch between development and release version of special directories. The currently not used version is kept in a cache.
# parameter: Select, if the release version (switchRelease) or the development (switchDevel) version should be activated
#####
switchSpecialDirs()	# git
{
	mode=$1
	export err=0

	case $mode in
		"switchRelease")
			export wantedOrigState='devel'
			export wantedCacheState='release'
		;;
		"switchDevel")
			export wantedOrigState='release'
			export wantedCacheState='devel'
		;;
		*)
			echo "Err: UNKNOWN mode \"$mode\"!"
			exit 5
		;;
	esac

	# Check, if the state of all cache directories is correctly
	for cacheStateFile in $(ls /mdk/specialDirs/*/releaseOrDevel)
	do
		# Check, if the state of this cache directory is correctly
		if [ $(cat "$cacheStateFile") != $wantedCacheState ]
		then
			echo "Err: \"$(cat "$cacheStateFile")\" found in status file \"$cacheStateFile\" but \"$wantedCacheState\" required!"
			export err=$[ $err + 1 ]
		fi
	done

	# Check, if the state of all original directories is correctly
	for origDir in $specialDirs
	do
		origStateFile="$origDir/releaseOrDevel"
	
		# Check, if the state of the original directory is correctly
		if [ $(cat "$origStateFile") != $wantedOrigState ]
		then
			echo "Err: \"$(cat "$origStateFile")\" found in status file \"$origStateFile\" but \"$wantedOrigState\" required!"
			read -p 'Press Enter'
			exit 5
		fi
	done

	# Exit if there is any mismatch
	if [ $err -gt 0 ]
	then
		read -p 'Press Enter'
		exit 3
	fi

	# Stop Apache, TFTP and MariaDB servers
	stopDaemons
	
	for origDir in $specialDirs
	do
		origStateFile="$origDir/releaseOrDevel"

		# Calculate the name of the directory where the version is stored that currently is NOT in use
		cacheDir="$(origDir2CacheDir $origDir)"
		cacheStateFile="$cacheDir/releaseOrDevel"

		# Check existence of the cache dir
		if [ ! -d "$cacheDir" ]
		then
			echo "Err: Cache dir \"$cacheDir\" does NOT exist!"
			read -p 'Press Enter'
			exit 1
		fi

		# Check, if the state file for the cache is there
		if [ ! -f "$cacheStateFile" ]
		then
			echo "Err: Status file \"$cacheStateFile\" MISSING!"
			read -p 'Press Enter'
			exit 2
		fi

		# Check, if the state of the cache directory is correctly
		if [ $(cat "$cacheStateFile") != $wantedCacheState ]
		then
			echo "Err: \"$(cat "$cacheStateFile")\" found in status file \"$cacheStateFile\" but \"$wantedCacheState\" required!"
			read -p 'Press Enter'
			exit 3
		fi

		# Check, if the state file in the original directory is there
		if [ ! -f  "$origStateFile" ]
		then
			echo "Err: Status file \"$origStateFile\" MISSING!"
			read -p 'Press Enter'
			exit 4
		fi

		# Check, if the state of the original directory is correctly
		if [ $(cat "$origStateFile") != $wantedOrigState ]
		then
			echo "Err: \"$(cat "$origStateFile")\" found in status file \"$origStateFile\" but \"$wantedOrigState\" required!"
			read -p 'Press Enter'
			exit 5
		fi

		# eg. mv /mdk/specialDirs/mdk-server-iso-pool /mdk/specialDirs/mdk-server-iso-pool-newOrig
		mv -v "$cacheDir" "$cacheDir-newOrig"

		# eg. mv /mdk/server/iso/pool /mdk/specialDirs/mdk-server-iso-pool
		mv -v "$origDir" "$cacheDir"

		# eg. mv /mdk/specialDirs/mdk-server-iso-pool-newOrig /mdk/server/iso/pool
		mv -v "$cacheDir-newOrig" "$origDir"
	done

	# Start Apache, TFTP and MariaDB servers
	startDaemons
}