LOCK TABLES `sourceslist` WRITE;
DELETE FROM `sourceslist` WHERE name="unstable";
INSERT INTO `sourceslist` (`name`, `distr`, `release`, `description`, `list`, `desktops`, `archs`) VALUES ('unstable', 'debian', 'sid', 'for Debian/Unstable', '#!!!Please do not edit this sources list. Changes will get lost on the next software update. Create a new sources list instead.!!!
#!!!Bitte editieren Sie nicht diese Paketquellenliste. Ihre �nderungen w�rden sonst bei der n�chsten Softwareaktualisierung verlorengehen. Erstellen Sie stattdessen bitte eine neue Paketquellenliste.!!!

#mirror: ftp://ftp.de.debian.org/debian
#alternativeFS: ext4
#supportedFS: ext2, ext3, ext4, reiserfs

#unstable sources
deb-src http://ftp2.de.debian.org/debian/ unstable main non-free contrib
deb http://ftp2.de.debian.org/debian/ unstable main non-free contrib

#knoppix packages: hwsetup, hwdata-knoppix, ddcxinfo-knoppix, xf86config-knoppix
deb http://switch.dl.sourceforge.net/project/m23/m23debs/ ./
#deb http://m23.sourceforge.net/m23debs/ ./', 'Textmode###KDE3###gnome2###X###XFce', 'i386###amd64');
DELETE FROM `sourceslist` WHERE name="testing";
INSERT INTO `sourceslist` (`name`, `distr`, `release`, `description`, `list`, `desktops`, `archs`) VALUES ('testing', 'debian', 'sarge', 'for Debian/Testing', '#!!!Please do not edit this sources list. Changes will get lost on the next software update. Create a new sources list instead.!!!
#!!!Bitte editieren Sie nicht diese Paketquellenliste. Ihre �nderungen w�rden sonst bei der n�chsten Softwareaktualisierung verlorengehen. Erstellen Sie stattdessen bitte eine neue Paketquellenliste.!!!

#mirror: ftp://ftp.de.debian.org/debian
#alternativeFS: ext4
#supportedFS: ext2, ext3, ext4, reiserfs

#testing sources
deb-src http://ftp.de.debian.org/debian/ testing main non-free contrib
deb http://ftp.de.debian.org/debian/ testing main non-free contrib

#knoppix packages: hwsetup, hwdata-knoppix, ddcxinfo-knoppix, xf86config-knoppix
deb http://switch.dl.sourceforge.net/project/m23/m23debs/ ./
#deb http://m23.sourceforge.net/m23debs/ ./', 'Textmode###KDE3###gnome2###X###XFce', 'i386###amd64');
DELETE FROM `sourceslist` WHERE name="HS-Fedora14";
INSERT INTO `sourceslist` (`name`, `distr`, `release`, `description`, `list`, `desktops`, `archs`) VALUES ('HS-Fedora14', 'halfSister', 'fedora14', '', '#alternativeFS: ext4
#supportedFS: ext2, ext3, ext4, reiserfs
[fedora]
name=Fedora $releasever - $basearch
failovermethod=priority
#baseurl=http://download.fedoraproject.org/pub/fedora/linux/releases/$releasever/Everything/$basearch/os/
mirrorlist=https://mirrors.fedoraproject.org/metalink?repo=fedora-$releasever&arch=$basearch
enabled=1
metadata_expire=7d
gpgcheck=1
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-fedora-$basearch

[fedora-debuginfo]
name=Fedora $releasever - $basearch - Debug
failovermethod=priority
#baseurl=http://download.fedoraproject.org/pub/fedora/linux/releases/$releasever/Everything/$basearch/debug/
mirrorlist=https://mirrors.fedoraproject.org/metalink?repo=fedora-debug-$releasever&arch=$basearch
enabled=0
metadata_expire=7d
gpgcheck=1
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-fedora-$basearch

[fedora-source]
name=Fedora $releasever - Source
failovermethod=priority
#baseurl=http://download.fedoraproject.org/pub/fedora/linux/releases/$releasever/Everything/source/SRPMS/
mirrorlist=https://mirrors.fedoraproject.org/metalink?repo=fedora-source-$releasever&arch=$basearch
enabled=0
metadata_expire=7d
gpgcheck=1
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-fedora-$basearch
[updates]
name=Fedora $releasever - $basearch - Updates
failovermethod=priority
#baseurl=http://download.fedoraproject.org/pub/fedora/linux/updates/$releasever/$basearch/
mirrorlist=https://mirrors.fedoraproject.org/metalink?repo=updates-released-f$releasever&arch=$basearch
enabled=1
gpgcheck=1
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-fedora-$basearch

[updates-debuginfo]
name=Fedora $releasever - $basearch - Updates - Debug
failovermethod=priority
#baseurl=http://download.fedoraproject.org/pub/fedora/linux/updates/$releasever/$basearch/debug/
mirrorlist=https://mirrors.fedoraproject.org/metalink?repo=updates-released-debug-f$releasever&arch=$basearch
enabled=0
gpgcheck=1
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-fedora-$basearch

[updates-source]
name=Fedora $releasever - Updates Source
failovermethod=priority
#baseurl=http://download.fedoraproject.org/pub/fedora/linux/updates/$releasever/SRPMS/
mirrorlist=https://mirrors.fedoraproject.org/metalink?repo=updates-released-source-f$releasever&arch=$basearch
enabled=0
gpgcheck=1
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-fedora-$basearch
[updates-testing]
name=Fedora $releasever - $basearch - Test Updates
failovermethod=priority
#baseurl=http://download.fedoraproject.org/pub/fedora/linux/updates/testing/$releasever/$basearch/
mirrorlist=https://mirrors.fedoraproject.org/metalink?repo=updates-testing-f$releasever&arch=$basearch
enabled=0
gpgcheck=1
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-fedora-$basearch

[updates-testing-debuginfo]
name=Fedora $releasever - $basearch - Test Updates Debug
failovermethod=priority
#baseurl=http://download.fedoraproject.org/pub/fedora/linux/updates/testing/$releasever/$basearch/debug/
mirrorlist=https://mirrors.fedoraproject.org/metalink?repo=updates-testing-debug-f$releasever&arch=$basearch
enabled=0
gpgcheck=1
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-fedora-$basearch

[updates-testing-source]
name=Fedora $releasever - Test Updates Source
failovermethod=priority
#baseurl=http://download.fedoraproject.org/pub/fedora/linux/updates/testing/$releasever/SRPMS/
mirrorlist=https://mirrors.fedoraproject.org/metalink?repo=updates-testing-source-f$releasever&arch=$basearch
enabled=0
gpgcheck=1
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-fedora-$basearch', 'Textmode###KDE###Gnome###XFce###LXDE', 'i386');
DELETE FROM `sourceslist` WHERE name="HS-opensuse11.4";
INSERT INTO `sourceslist` (`name`, `distr`, `release`, `description`, `list`, `desktops`, `archs`) VALUES ('HS-opensuse11.4', 'halfSister', 'opensuse11.4', '', '#alternativeFS: ext4
#supportedFS: ext2, ext3, ext4, reiserfs
[repo-oss]
name=repo-oss
enabled=1
autorefresh=0
baseurl=http://download.opensuse.org/distribution/11.4/repo/oss/
type=yast2
keeppackages=0

[update]
name=update
enabled=1
autorefresh=1
baseurl=http://download.opensuse.org/update/11.4/
type=NONE
keeppackages=0', 'Textmode###KDE###Gnome###XFce', 'i386');
DELETE FROM `sourceslist` WHERE name="HS-centos62";
INSERT INTO `sourceslist` (`name`, `distr`, `release`, `description`, `list`, `desktops`, `archs`) VALUES ('HS-centos62', 'halfSister', 'CentOS6.2', 'CentOS 6.2', '#alternativeFS: ext4
#supportedFS: ext2, ext3, ext4, reiserfs

[base]
name=CentOS-$releasever - Base
mirrorlist=http://mirrorlist.centos.org/?release=$releasever&arch=$basearch&repo=os
#baseurl=http://mirror.centos.org/centos/$releasever/os/$basearch/
gpgcheck=1
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-CentOS-6

#released updates
[updates]
name=CentOS-$releasever - Updates
mirrorlist=http://mirrorlist.centos.org/?release=$releasever&arch=$basearch&repo=updates
#baseurl=http://mirror.centos.org/centos/$releasever/updates/$basearch/
gpgcheck=1
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-CentOS-6

#additional packages that may be useful
[extras]
name=CentOS-$releasever - Extras
mirrorlist=http://mirrorlist.centos.org/?release=$releasever&arch=$basearch&repo=extras
#baseurl=http://mirror.centos.org/centos/$releasever/extras/$basearch/
gpgcheck=1
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-CentOS-6

#additional packages that extend functionality of existing packages
[centosplus]
name=CentOS-$releasever - Plus
mirrorlist=http://mirrorlist.centos.org/?release=$releasever&arch=$basearch&repo=centosplus
#baseurl=http://mirror.centos.org/centos/$releasever/centosplus/$basearch/
gpgcheck=1
enabled=0
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-CentOS-6

#contrib - packages by Centos Users
[contrib]
name=CentOS-$releasever - Contrib
mirrorlist=http://mirrorlist.centos.org/?release=$releasever&arch=$basearch&repo=contrib
#baseurl=http://mirror.centos.org/centos/$releasever/contrib/$basearch/
gpgcheck=1
enabled=0
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-CentOS-6

[epel]
name=Extra Packages for Enterprise Linux 6 - $basearch
#baseurl=http://download.fedoraproject.org/pub/epel/6/$basearch
mirrorlist=https://mirrors.fedoraproject.org/metalink?repo=epel-6&arch=$basearch
failovermethod=priority
enabled=1
gpgcheck=1
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-EPEL-6

[epel-debuginfo]
name=Extra Packages for Enterprise Linux 6 - $basearch - Debug
#baseurl=http://download.fedoraproject.org/pub/epel/6/$basearch/debug
mirrorlist=https://mirrors.fedoraproject.org/metalink?repo=epel-debug-6&arch=$basearch
failovermethod=priority
enabled=0
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-EPEL-6
gpgcheck=1

[epel-source]
name=Extra Packages for Enterprise Linux 6 - $basearch - Source
#baseurl=http://download.fedoraproject.org/pub/epel/6/SRPMS
mirrorlist=https://mirrors.fedoraproject.org/metalink?repo=epel-source-6&arch=$basearch
failovermethod=priority
enabled=0
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-EPEL-6
gpgcheck=1', 'Textmode###KDE###Gnome###XFce', 'i386###amd64');
DELETE FROM `sourceslist` WHERE name="jessie";
INSERT INTO `sourceslist` (`name`, `distr`, `release`, `description`, `list`, `desktops`, `archs`) VALUES ('jessie', 'debian', 'jessie', 'for Debian 8.x Jessie', '#!!!Please do not edit this sources list. Changes will get lost on the next software update. Create a new sources list instead.!!!
#!!!Bitte editieren Sie nicht diese Paketquellenliste. Ihre �nderungen w�rden sonst bei der n�chsten Softwareaktualisierung verlorengehen. Erstellen Sie stattdessen bitte eine neue Paketquellenliste.!!!

#mirror: ftp://ftp.de.debian.org/debian
#alternativeFS: ext4
#supportedFS: ext2, ext3, ext4, reiserfs

deb http://ftp.de.debian.org/debian/ jessie main non-free contrib
deb http://security.debian.org/ jessie/updates main contrib non-free

#knoppix packages: hwsetup, hwdata-knoppix, ddcxinfo-knoppix, xf86config-knoppix
deb http://vorboss.dl.sourceforge.net/project/m23/m23debs/ ./

deb http://mirror.ppa.trinitydesktop.org/trinity/trinity-r14.0.0/debian jessie main
deb http://mirror.ppa.trinitydesktop.org/trinity/trinity-builddeps-r14.0.0/debian jessie main', 'Textmode###Trinity###X###Debian8Mate###Debian8MateFull###Debian8CinnamonFull###Debian8GnomeFull###Debian8KdeFull###Debian8LxdeFull###Debian8XfceFull', 'amd64###i386');
DELETE FROM `sourceslist` WHERE name="Ubuntu-Xenial";
INSERT INTO `sourceslist` (`name`, `distr`, `release`, `description`, `list`, `desktops`, `archs`) VALUES ('Ubuntu-Xenial', 'ubuntu', 'xenial', 'Ubuntu 16.04 LTS Xenial Xerus', '#!!!Please do not edit this sources list. Changes will get lost on the next software update. Create a new sources list instead.!!!
#!!!Bitte editieren Sie nicht diese Paketquellenliste. Ihre �nderungen w�rden sonst bei der n�chsten Softwareaktualisierung verlorengehen. Erstellen Sie stattdessen bitte eine neue Paketquellenliste.!!!

#mirror: ftp://archive.ubuntu.com/ubuntu
#alternativeFS: ext4
#supportedFS: ext2, ext3, ext4, reiserfs
#supportedEFI: amd64

#Xenial sources
deb http://de.archive.ubuntu.com/ubuntu/ xenial main restricted universe multiverse
deb http://de.archive.ubuntu.com/ubuntu/ xenial-updates main restricted universe multiverse
deb http://de.archive.ubuntu.com/ubuntu/ xenial-backports main restricted universe multiverse
deb http://security.ubuntu.com/ubuntu xenial-security main restricted universe multiverse

#knoppix packages: hwsetup, hwdata-knoppix, ddcxinfo-knoppix, xf86config-knoppix
deb http://m23debs.goos-habermann.de ./

#Trinity
deb http://mirror.ppa.trinitydesktop.org/trinity/trinity-r14.0.0/ubuntu xenial main
deb http://mirror.ppa.trinitydesktop.org/trinity/trinity-builddeps-r14.0.0/ubuntu xenial main', 'Textmode###X###Unity3D1604###Unity3DMinimal1604###Ubuntu1604Mate###UbuntuXubuntu1604###UbuntuGnome1604###UbuntuLubuntu1604###UbuntuKubuntu1604###Ubuntu1604Trinity', 'amd64###i386');
DELETE FROM `sourceslist` WHERE name="LinuxMint 18 Sarah";
INSERT INTO `sourceslist` (`name`, `distr`, `release`, `description`, `list`, `desktops`, `archs`) VALUES ('LinuxMint 18 Sarah', 'ubuntu', 'xenial', 'LinuxMint 18 Sarah', '#!!!Please do not edit this sources list. Changes will get lost on the next software update. Create a new sources list instead.!!!
#!!!Bitte editieren Sie nicht diese Paketquellenliste. Ihre �nderungen w�rden sonst bei der n�chsten Softwareaktualisierung verlorengehen. Erstellen Sie stattdessen bitte eine neue Paketquellenliste.!!!

#mirror: ftp://archive.ubuntu.com/ubuntu
#alternativeFS: ext4
#supportedFS: ext2, ext3, ext4, reiserfs
#supportedEFI: amd64
#addToFile:/etc/apt/preferences.d/official-package-repositories.pref###Package: *### Pin: origin live.linuxmint.com### Pin-Priority: 750### ### Package: *### Pin: release o=linuxmint,c=upstream### Pin-Priority: 700### ### Package: *### Pin: release o=Ubuntu### Pin-Priority: 500
#addToXXXFile:/etc/apt/preferences.d/official-extra-repositories.pref###Package: *### Pin: origin build.linuxmint.com### Pin-Priority: 700###
#addToFile:/etc/apt/apt.conf.d/00recommends###APT::Install-Recommends "false";###Aptitude::Recommends-Important "false";


deb http://packages.linuxmint.com sarah main upstream import backport #id:linuxmint_main

deb http://archive.ubuntu.com/ubuntu xenial main restricted universe multiverse
deb http://archive.ubuntu.com/ubuntu xenial-updates main restricted universe multiverse
deb http://archive.ubuntu.com/ubuntu xenial-backports main restricted universe multiverse

deb http://security.ubuntu.com/ubuntu/ xenial-security main restricted universe multiverse
deb http://archive.canonical.com/ubuntu/ xenial partner

deb http://ppa.launchpad.net/kubuntu-ppa/backports/ubuntu xenial main

#knoppix packages: hwsetup, hwdata-knoppix, ddcxinfo-knoppix, xf86config-knoppix
deb http://m23debs.goos-habermann.de ./', 'Textmode###Mint18KDE###Mint18Mate###Mint18Cinnamon###Mint18Xfce', 'amd64###i386');
DELETE FROM `sourceslist` WHERE name="LinuxMint 18.1 Serena";
INSERT INTO `sourceslist` (`name`, `distr`, `release`, `description`, `list`, `desktops`, `archs`) VALUES ('LinuxMint 18.1 Serena', 'ubuntu', 'xenial', 'LinuxMint 18.1 Serena', '#!!!Please do not edit this sources list. Changes will get lost on the next software update. Create a new sources list instead.!!!
#!!!Bitte editieren Sie nicht diese Paketquellenliste. Ihre �nderungen w�rden sonst bei der n�chsten Softwareaktualisierung verlorengehen. Erstellen Sie stattdessen bitte eine neue Paketquellenliste.!!!

#mirror: ftp://archive.ubuntu.com/ubuntu
#alternativeFS: ext4
#supportedFS: ext2, ext3, ext4, reiserfs
#supportedEFI: amd64
#addToFile:/etc/apt/preferences.d/official-package-repositories.pref###Package: *### Pin: origin live.linuxmint.com### Pin-Priority: 750### ### Package: *### Pin: release o=linuxmint,c=upstream### Pin-Priority: 700### ### Package: *### Pin: release o=Ubuntu### Pin-Priority: 500
#addToXXXFile:/etc/apt/preferences.d/official-extra-repositories.pref###Package: *### Pin: origin build.linuxmint.com### Pin-Priority: 700###
#addToFile:/etc/apt/apt.conf.d/00recommends###APT::Install-Recommends "false";###Aptitude::Recommends-Important "false";


deb http://packages.linuxmint.com serena main upstream import backport #id:linuxmint_main

deb http://archive.ubuntu.com/ubuntu xenial main restricted universe multiverse
deb http://archive.ubuntu.com/ubuntu xenial-updates main restricted universe multiverse
deb http://archive.ubuntu.com/ubuntu xenial-backports main restricted universe multiverse

deb http://security.ubuntu.com/ubuntu/ xenial-security main restricted universe multiverse
deb http://archive.canonical.com/ubuntu/ xenial partner

deb http://ppa.launchpad.net/kubuntu-ppa/backports/ubuntu xenial main

#knoppix packages: hwsetup, hwdata-knoppix, ddcxinfo-knoppix, xf86config-knoppix
deb http://m23debs.goos-habermann.de ./', 'Textmode###Mint18KDE###Mint18Mate###Mint18Cinnamon###Mint18Xfce', 'amd64###i386');
DELETE FROM `sourceslist` WHERE name="devuanjessie";
INSERT INTO `sourceslist` (`name`, `distr`, `release`, `description`, `list`, `desktops`, `archs`) VALUES ('devuanjessie', 'debian', 'devuanjessie', 'for Devuan Jessie 1.0', '#!!!Please do not edit this sources list. Changes will get lost on the next software update. Create a new sources list instead.!!!
#!!!Bitte editieren Sie nicht diese Paketquellenliste. Ihre �nderungen w�rden sonst bei der n�chsten Softwareaktualisierung verlorengehen. Erstellen Sie stattdessen bitte eine neue Paketquellenliste.!!!

#mirror: http://de.mirror.devuan.org/merged/
#alternativeFS: ext4
#supportedFS: ext2, ext3, ext4, reiserfs

deb http://de.mirror.devuan.org/devuan/ jessie main contrib non-free

# jessie-security, previously known as \'volatile\'
deb http://de.mirror.devuan.org/devuan/ jessie-security main non-free contrib

# jessie-updates, previously known as \'volatile\'
deb http://de.mirror.devuan.org/devuan/ jessie-updates main non-free contrib

#knoppix packages: hwsetup, hwdata-knoppix, ddcxinfo-knoppix, xf86config-knoppix
deb http://m23debs.goos-habermann.de ./

deb http://mirror.ppa.trinitydesktop.org/trinity/trinity-r14.0.0/debian jessie main
deb http://mirror.ppa.trinitydesktop.org/trinity/trinity-builddeps-r14.0.0/debian jessie main', 'Textmode###Trinity###X###Debian8Mate###Debian8MateFull###Debian8LxdeFull###Debian8XfceFull', 'amd64###i386');
DELETE FROM `sourceslist` WHERE name="LinuxMint 18.2 Sonya";
INSERT INTO `sourceslist` (`name`, `distr`, `release`, `description`, `list`, `desktops`, `archs`) VALUES ('LinuxMint 18.2 Sonya', 'ubuntu', 'xenial', 'LinuxMint 18.2 Sonya', '#!!!Please do not edit this sources list. Changes will get lost on the next software update. Create a new sources list instead.!!!
#!!!Bitte editieren Sie nicht diese Paketquellenliste. Ihre �nderungen w�rden sonst bei der n�chsten Softwareaktualisierung verlorengehen. Erstellen Sie stattdessen bitte eine neue Paketquellenliste.!!!

#mirror: ftp://archive.ubuntu.com/ubuntu
#alternativeFS: ext4
#supportedFS: ext2, ext3, ext4, reiserfs
#supportedEFI: amd64
#addToFile:/etc/apt/preferences.d/official-package-repositories.pref###Package: *### Pin: origin live.linuxmint.com### Pin-Priority: 750### ### Package: *### Pin: release o=linuxmint,c=upstream### Pin-Priority: 700### ### Package: *### Pin: release o=Ubuntu### Pin-Priority: 500
#addToXXXFile:/etc/apt/preferences.d/official-extra-repositories.pref###Package: *### Pin: origin build.linuxmint.com### Pin-Priority: 700###
#addToFile:/etc/apt/apt.conf.d/00recommends###APT::Install-Recommends "false";###Aptitude::Recommends-Important "false";


deb http://packages.linuxmint.com sonya main upstream import backport #id:linuxmint_main

deb http://archive.ubuntu.com/ubuntu xenial main restricted universe multiverse
deb http://archive.ubuntu.com/ubuntu xenial-updates main restricted universe multiverse
deb http://archive.ubuntu.com/ubuntu xenial-backports main restricted universe multiverse

deb http://security.ubuntu.com/ubuntu/ xenial-security main restricted universe multiverse
deb http://archive.canonical.com/ubuntu/ xenial partner

deb http://ppa.launchpad.net/kubuntu-ppa/backports/ubuntu xenial main

#knoppix packages: hwsetup, hwdata-knoppix, ddcxinfo-knoppix, xf86config-knoppix
deb http://m23debs.goos-habermann.de ./', 'Textmode###Mint18KDE###Mint18Mate###Mint18Cinnamon###Mint18Xfce', 'amd64###i386');
DELETE FROM `sourceslist` WHERE name="buster@pi5";
INSERT INTO `sourceslist` (`name`, `distr`, `release`, `description`, `list`, `desktops`, `archs`) VALUES ('buster@pi5', 'debian', 'buster', 'for Debian 10.x Buster', '#!!!Please do not edit this sources list. Changes will get lost on the next software update. Create a new sources list instead.!!!
#!!!Bitte editieren Sie nicht diese Paketquellenliste. Ihre �nderungen w�rden sonst bei der n�chsten Softwareaktualisierung verlorengehen. Erstellen Sie stattdessen bitte eine neue Paketquellenliste.!!!

#mirror: ftp://ftp.de.debian.org/debian
#alternativeFS: ext4
#supportedFS: ext2, ext3, ext4, reiserfs

deb [arch=amd64] http://192.168.1.5/debian/deb.debian.org/debian/ buster main non-free contrib
deb [arch=amd64] http://192.168.1.5/debian/security.debian.org/debian-security/ buster/updates main contrib non-free

#knoppix packages: hwsetup, hwdata-knoppix, ddcxinfo-knoppix, xf86config-knoppix
#deb http://m23debs.goos-habermann.de ./', 'Textmode###X###Debian8Mate###Debian8MateFull###Debian8CinnamonFull###Debian8GnomeFull###Debian8KdeFull###Debian8LxdeFull###Debian8XfceFull', 'amd64###i386');
DELETE FROM `sourceslist` WHERE name="busterGPG";
INSERT INTO `sourceslist` (`name`, `distr`, `release`, `description`, `list`, `desktops`, `archs`) VALUES ('busterGPG', 'debian', 'buster', 'for Debian 10.x Buster', '#!!!Please do not edit this sources list. Changes will get lost on the next software update. Create a new sources list instead.!!!
#!!!Bitte editieren Sie nicht diese Paketquellenliste. Ihre �nderungen w�rden sonst bei der n�chsten Softwareaktualisierung verlorengehen. Erstellen Sie stattdessen bitte eine neue Paketquellenliste.!!!

#mirror: ftp://ftp.de.debian.org/debian
#alternativeFS: ext4
#supportedFS: ext2, ext3, ext4, reiserfs
#supportedEFI: amd64
#importGPGKey:https://apt.ennit.net/repository.gpg.key
#importGPGKey:https://apt.ennit.net/repository.gpg.key2

deb http://ftp.de.debian.org/debian/ buster main non-free contrib
deb http://security.debian.org/ buster/updates main contrib non-free

#knoppix packages: hwsetup, hwdata-knoppix, ddcxinfo-knoppix, xf86config-knoppix
deb http://m23debs.goos-habermann.de ./

#deb http://mirror.ppa.trinitydesktop.org/trinity/trinity-r14.0.0/debian buster main
#deb http://mirror.ppa.trinitydesktop.org/trinity/trinity-builddeps-r14.0.0/debian buster main', 'Textmode###X###Debian8Mate###Debian8MateFull###Debian8CinnamonFull###Debian8GnomeFull###Debian8KdeFull###Debian8LxdeFull###Debian8XfceFull', 'amd64###i386');
UNLOCK TABLES;