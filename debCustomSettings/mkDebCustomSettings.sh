#!/bin/bash

cd /mdk/debCustomSettings

# Verzeichnis zum Bauen des Debian-Paketes anlegen
mkdir -p customsettingsm23server
cd customsettingsm23server

# Backupverzeichnis anlegen
mkdir /mdk/debCustomSettings/backup

# Unterverzeichnisse (neu) anlegen
rm -r m23/data+scripts/extraDebs m23/inc m23/m23customPatch 2> /dev/null
mkdir -p m23/data+scripts/extraDebs m23/inc m23/m23customPatch tmp/m23-install

# Hardlinks anlegen, damit die Dateien nicht doppelten Speicherplatz belegen, aber dennoch in die Dateistruktur aufgenommen werden
ln /m23/data+scripts/extraDebs/*.deb m23/data+scripts/extraDebs
ln /m23/inc/schoolInfoConf.php /m23/inc/schoolConfig.php m23/inc
ln /m23/m23customPatch/*.m23customPatch m23/m23customPatch

# Das Paket nicht in sich selbst verpacken
rm m23/data+scripts/extraDebs/customsettingsm23server*.deb

# Nicht-Standard-Paketquellelisten exportieren
php /mdk/bin/exportDBsourceslist.php nonStandardSourceslists > tmp/m23-install/sourceslist.sql

# Paketzusammenstellungen exportieren
dbuser=`/m23/bin/getMySQL-UserParam.sh`
dbadmpassParam=`/m23/bin/getMySQL-PasswordParam.sh`
mysqldump $dbuser $dbadmpassParam m23 recommendpackages > tmp/m23-install/packageselections.sql

# Paket bauen (mit Datum + Uhrzeit als Patchnummer)
dt_deb_mkDeb date

# Benutzer/Gruppe auf Apache �ndern
chown www-data:www-data ../customsettingsm23server*.deb

# Hardlink als Sicherung im Backup-Verzeichnis anlegen
ln ../customsettingsm23server*.deb /mdk/debCustomSettings/backup

# Nur die neuesten 7 Versionen des Debian-Paketes behalten, alle �lteren l�schen
ls -t1 /mdk/debCustomSettings/backup/customsettingsm23server*.deb | awk '{ if (NR > 7) system("rm "$0); }'

# Paket nach extraDebs verschieben
mv ../customsettingsm23server*.deb /m23/data+scripts/extraDebs

# Alte Paketversionen entfernen, soda� nur noch die neuste von jedem Paket vorhanden ist
sudo su - www-data -c ". /mdk/m23Debs/bin/m23Deb.inc; cd /m23/data+scripts/extraDebs/; deleteOldDoubles"

# Paketindex und Signierung aktualisieren
/m23/bin/m23cli.php indexAndSignExtraDebs

