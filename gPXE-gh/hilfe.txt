To create a bootable floppy, type
    cat bin/gpxe.dsk > /dev/fd0
where /dev/fd0 is your floppy drive.  This will erase any
data already on the disk.

To create a bootable USB key, type
    cat bin/gpxe.usb > /dev/sdX
where /dev/sdX is your USB key, and is *not* a real hard
disk on your system.  This will erase any data already on
the USB key.

To create a bootable CD-ROM, burn the ISO image
bin/gpxe.iso to a blank CD-ROM.

These images contain drivers for all supported cards.  You
can build more customised images, and ROM images, using
    make bin/<rom-name>.<output-format>


http://kernel.org/pub/software/utils/boot/gpxe/

cd src
make clean
make EMBEDDED_IMAGE=../../conf/m23shared.gpxe
make NO_WERROR=1 EMBEDDED_IMAGE=../../conf/m23shared-gh.gpxe
